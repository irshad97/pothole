import json
import numpy

from flask import Flask, Response, request, jsonify, send_from_directory, render_template
import sys
from flask_cors import CORS
import os

from detect_video_example import detect_video, get_pothole_list

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
CORS(app, resources=r'/*')


@app.route('/videoFile' , methods=['GET'])
def video_file():
	# print(request.files , file=sys.stderr)
	#path = '/home/root1/Documents/irshad/pot-hole/Object-Detection-Using-yolov4-tiny-main/converting_darknet_into_tflite/data/video/pothole-demo_new.mp4'
	path = "http://192.168.43.185:8080/video"
	weight = '/home/root1/Documents/irshad/pot-hole/Object-Detection-Using-yolov4-tiny-main/converting_darknet_into_tflite/checkpoints/yolov4-tiny-416'

	return Response(detect_video(path), mimetype='multipart/x-mixed-replace; boundary=frame')
##################################################### THE REAL DEAL HAPPENS ABOVE ######################################

@app.route('/test' , methods=['GET','POST'])
def test():
	print("log: got at test" , file=sys.stderr)
	return jsonify({'status':'succces'})

@app.route('/')
def home():
	return render_template('./index.html')

@app.route('/pothole_data' , methods=['GET'])
def pothole_detected_list():
	#print("inside pothole_list api")
	# return jsonify({'status':'succces'})
	return get_pothole_list()
	#return json.dumps({get_pothole_list(): numpy.int64(42)})

@app.after_request
def after_request(response):
    print("log: setting cors" , file = sys.stderr)
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


if __name__ == '__main__':
	app.run(debug = True)
