import json
import sys
from flask_cors import CORS
import time
import tensorflow as tf
physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
from absl import app, flags, logging
from absl.flags import FLAGS
import core.utils as utils
from core.yolov4 import filter_boxes
from tensorflow.python.saved_model import tag_constants
from PIL import Image
import cv2
import numpy as np
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

from wsgiref.util import FileWrapper
from flask import Flask, Response, request, jsonify, send_from_directory

flags.DEFINE_string('framework', 'tf', '(tf, tflite, trt')
flags.DEFINE_string('weights', './checkpoints/yolov4-tiny-416',
                    'path to weights file')
flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_boolean('tiny', True, 'yolo or yolo-tiny')
flags.DEFINE_string('model', 'yolov4', 'yolov3 or yolov4')
flags.DEFINE_string('video', './data/video/pothole-demo_new.mp4', 'path to input video or set to 0 for webcam')
flags.DEFINE_string('output', None, 'path to output video')
flags.DEFINE_string('output_format', 'MP4', 'codec used in VideoWriter when saving video to file')
flags.DEFINE_float('iou', 0.45, 'iou threshold')
flags.DEFINE_float('score', 0.25, 'score threshold')
flags.DEFINE_boolean('dont_show', False, 'dont show video output')

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
CORS(app, resources=r'/*')
pothole_list = []
global_dict = {}

# @app.route('/video' , methods=['GET'])
# def video():
#     video_path = '/home/root1/Documents/irshad/pot-hole/Object-Detection-Using-yolov4-tiny-main/converting_darknet_into_tflite/data/video/pothole-demo_new.mp4'
#     return Response(detect_video(video_path), mimetype='multipart/x-mixed-replace; boundary=frame')

def detect_video(video_path):
    pothole_list_local = []
    global pothole_list
    global global_dict
    #video_path = '/home/root1/Documents/irshad/pot-hole/Object-Detection-Using-yolov4-tiny-main/converting_darknet_into_tflite/data/video/pothole-demo_new.mp4'
    weights = '/home/root1/Documents/irshad/pot-hole/Object-Detection-Using-yolov4-tiny-main/converting_darknet_into_tflite/checkpoints/yolov4-tiny-416'
    tiny = True
    config = ConfigProto()
    config.gpu_options.allow_growth = True
    session = InteractiveSession(config=config)
    STRIDES, ANCHORS, NUM_CLASS, XYSCALE = utils.load_config_new(tiny, 'yolov4')
    input_size = 416
    video_path = video_path
    font = cv2.FONT_HERSHEY_PLAIN
    output = './data/video/new2.avi'
    output_format = 'XVID'
    framework = 'tf'
    iou = 0.45
    score = 0.25
    dont_show = True
    model = 'yolov4'

    if framework == 'tflite':
        interpreter = tf.lite.Interpreter(model_path=weights)
        interpreter.allocate_tensors()
        input_details = interpreter.get_input_details()
        output_details = interpreter.get_output_details()
        print(input_details)
        print(output_details)
    else:
        saved_model_loaded = tf.saved_model.load(weights, tags=[tag_constants.SERVING])
        infer = saved_model_loaded.signatures['serving_default']
        print(infer.structured_outputs)
        print(list(saved_model_loaded.signatures.keys()))

    # begin video capture
    try:
        vid = cv2.VideoCapture(int(video_path))
    except:
        vid = cv2.VideoCapture(video_path)

    out = None

    if output:
        # by default VideoCapture returns float instead of int
        width = int(vid.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps = int(vid.get(cv2.CAP_PROP_FPS))
        codec = cv2.VideoWriter_fourcc(*output_format)
        out = cv2.VideoWriter(output, codec, fps, (width, height))

    while True:
        return_value, frame = vid.read()
        if return_value:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            image = Image.fromarray(frame)
        else:
            print('Video has ended or failed, try a different video format!')
            break
    
        frame_size = frame.shape[:2]
        image_data = cv2.resize(frame, (input_size, input_size))
        image_data = image_data / 255.
        image_data = image_data[np.newaxis, ...].astype(np.float32)
        start_time = time.time()

        if framework == 'tflite':
            interpreter.set_tensor(input_details[0]['index'], image_data)
            interpreter.invoke()
            pred = [interpreter.get_tensor(output_details[i]['index']) for i in range(len(output_details))]
            if model == 'yolov3' and tiny == True:
                boxes, pred_conf = filter_boxes(pred[1], pred[0], score_threshold=0.25,
                                                input_shape=tf.constant([input_size, input_size]))
            else:
                boxes, pred_conf = filter_boxes(pred[0], pred[1], score_threshold=0.25,
                                                input_shape=tf.constant([input_size, input_size]))
        else:
            batch_data = tf.constant(image_data)
            pred_bbox = infer(batch_data)
            #print("infer",infer['detection_classes'])
            #print("pred_bbox",pred_bbox)
            #print(pred_bbox.values())
            for key, value in pred_bbox.items():
                boxes = value[:, :, 0:4]
                #print("boxes",boxes)
                pred_conf = value[:, :, 4:]
                #print("pred_cnfi", pred_conf)



        boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
            boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
            scores=tf.reshape(
                pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
            max_output_size_per_class=50,
            max_total_size=50,
            iou_threshold=iou,
            score_threshold=score
        )
        #print("classes", classes)
        pred_bbox = [boxes.numpy(), scores.numpy(), classes.numpy(), valid_detections.numpy()]
        #print("pred_box",pred_bbox)
        image, pothole_list = utils.draw_bbox_new(frame, pred_bbox, pothole_list)
        #get_pothole_list(pothole_list)
        #print(global_dict)
        #print(pothole_list)
        fps = 1.0 / (time.time() - start_time)
        cv2.putText(frame, "FPS: " + str(round(fps, 2)), (10, 50), font, 3, (255, 0, 0), 3)
        #print("FPS: %.2f" % fps)
        #print("image", image)
        result = np.asarray(image)
        #cv2.namedWindow("result", cv2.WINDOW_AUTOSIZE)
        result = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        #print("result", result)


        # _, jpeg = cv2.imencode('.jpg', frame)
        # yield (b'--frame\r\n'
        #        b'Content-Type: image/jpeg\r\n\r\n' + jpeg.tobytes() + b'\r\n\r\n')
        #Response(FileWrapper(open('./data/', 'rb')), mimetype='video/mp4')

        
        # if not dont_show:
        #     cv2.imshow("result", result)
        
        if output:
            out.write(result)

        if cv2.waitKey(1) & 0xFF == ord('q'): break

        _, jpeg = cv2.imencode('.jpg', frame)
        time.sleep(.01)
        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + jpeg.tobytes() + b'\r\n\r\n')
        #Response(FileWrapper(open('./data/video/new2.avi', 'rb')), mimetype='video/msvideo')



    #cv2.destroyAllWindows()

def get_pothole_list():
    #print(pothole_list)
    # for key, value in global_dict.items():
    #     print(key, value, type(value))
    #return json.dumps({global_dict: np.int64(42)}, default=convert)
    return jsonify(pothole_list[:-10:-1])


def convert(o):
    if isinstance(o, np.int64): return int(o)
    raise TypeError

@app.after_request
def after_request(response):
    print("log: setting cors" , file = sys.stderr)
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
    return response


if __name__ == '__main__':
	app.run(debug = True)
